package lesson7.homework;

import java.util.Date;

public class AnimalService {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setAnimalID(1);
        cat.setName("Barsik");
        Date data = new Date();
        cat.setData(data);
        cat.setEyesColor("Green");

        Dog dog = new Dog();
        dog.setAnimalID(2);
        dog.setName("Sharik");
        dog.setData(data);
        dog.setWeight(56.23);

        Tiger tiger = new Tiger();
        tiger.setAnimalID(3);
        tiger.setName("Tigra");
        tiger.setData(data);
        tiger.setEyesColor("brown");
        tiger.setCountEatenExployees(8);

        callPrint(cat);
        callPrint(dog);
        callPrint(tiger);
    }
    private static void callPrint (Animal animal){
        animal.printInfo();
    }
}
