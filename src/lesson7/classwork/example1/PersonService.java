package lesson7.classwork.example1;

public class PersonService {
    public static void main(String[] args) {
        exampleTwoForAbstractClass();
    }

    /**
     * Пример с абстрактными классами
     */
    private static void exampleTwoForAbstractClass(){
        Client client = new Client("Andrew","Pashkov", "SBER");
        client.displayInfo();

        Employee employee = new Employee("Pert","Chich", "SBER");
        employee.displayInfo();
    }

    /**
     * Объеты person можем создавать, пока класс не абстрактный
     */
//    private static void exampleWorkWhenPersonNotAbstract(){
//        Person person = new Person();
//        System.out.println("Person 1");
//        System.out.println(person.getName());
//        System.out.println(person.getSurname());
//
//        Person person2 = new Person("Vasiliy","Petrov");
//        System.out.println("Person 2");
//        System.out.println(person2.getName());
//        System.out.println(person2.getSurname());
//
//        Person person3 = new Person("Alexey","Ivanov");
//        System.out.println("Person 3");
//        System.out.println(person3.getName());
//        System.out.println(person3.getSurname());
//
//        Employee employee = new Employee("Igor","Sikor", "MTZ");
//        System.out.println("Employee");
//        System.out.println(employee.getName());
//        System.out.println(employee.getSurname());
//
//        Employee employee2 = new Employee("Micha","Patushkin", "MTZ");
//        System.out.println("Employee 2");
//        employee2.displayInfo();
//
//        // Первый спосоп вывести компанию
//        Employee employee3 = new Employee("Fedor","Kilov", "MTZ");
//        System.out.println("Employee 3");
//        System.out.println(employee3.getName());
//        System.out.println(employee3.getSurname());
//        System.out.println(employee3.getCompany());
//
//        // Второй способ вывести компанию (переопределение через @override)
//        Employee employee4 = new Employee("Andrew","Pashkov", "MTZ");
//        employee4.displayInfo();
//    }
}
