package lesson7.classwork.example1;

public abstract class Person {
    protected String name;
    protected String surname;

    public Person(){
    }
    public Person(String inputParamName, String inputParamSurname){
        name = inputParamName;
        surname = inputParamSurname;
    }

    public String getName() {
        return name == null ? "Name not found" : name;
    }

    public String getSurname(){
        return surname == null ? "Surname not found" : surname;
    }

    public abstract void displayInfo();
}
