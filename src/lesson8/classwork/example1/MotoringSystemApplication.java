package lesson8.classwork.example1;

public class MotoringSystemApplication {


    public static void main(String[] args) {
        MonitoringSystem monitoringSystem = new MonitoringSystem() {
           @Override
           public void startMonitoring(){
               System.out.println("Monitoring has been starting");
           }
        };
        monitoringSystem.startMonitoring();
    }
}
