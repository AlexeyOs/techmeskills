package lesson8.classwork.example3;

import java.util.Scanner;

public class DayApplication {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inputParam = scanner.nextInt();
        System.out.println(DayWeeks.getValueOfDayWeekByNumber(inputParam));
    }

}
