package lesson8.classwork.example3;

public enum DayWeeks {
    MONDAY(1,"Понедельник"), // Объекты
    TUESDAY(2, "Вторник"),
    WEDNESDAY(3, "Среда"),
    THURSDAY(4,"Четверг"),
    FRIDAY(5,"Пятница"),
    SATURDAY(6,"Суббота"),
    SUNDAY(7,"Воскресенье");
    int numberOfDayWeek; // Свойства
    String nameOfDayWeek;

    // Конструктор
    DayWeeks(int inputDayNumbers, String inputNameOfDayWeek){
        numberOfDayWeek = inputDayNumbers;
        nameOfDayWeek = inputNameOfDayWeek;
    }

    //Статичный метод, к которому обращаться только через класс, т.е. DayWeeks.
    public static String getValueOfDayWeekByNumber(int parametrFromScanner){
        DayWeeks[] dayWeeks = DayWeeks.values(); //У enum, есть методы values, который возвращает объекты в массив
        for(DayWeeks dayWeek : dayWeeks){
            if(dayWeek.numberOfDayWeek == parametrFromScanner){
                return dayWeek.nameOfDayWeek;
            }
        }
        return null;
    }

}
