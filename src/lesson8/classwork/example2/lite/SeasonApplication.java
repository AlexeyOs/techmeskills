package lesson8.classwork.example2.lite;
public class SeasonApplication {
    public static void main(String[] args) {
        //Пример 1 Получили объект SUMMER и поместили его в objSeason. Свойств в данном примере, у данного объекта нет.
        Season objSeason = Season.SUMMER;//objSeason - объект, который вернул Season.SUMMER
        System.out.println(objSeason);

        //Пример 2 Получили объект AUTUMN и сразу вывели его
        System.out.println(Season.AUTUMN);
    }

}
