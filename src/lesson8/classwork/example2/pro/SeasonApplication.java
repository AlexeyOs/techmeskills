package lesson8.classwork.example2.pro;

public class SeasonApplication {
    public static void main(String[] args) {
        //Пример 1 Получили объект SUMMER и поместили его в objSeason. Данный объект имеет свойство seasonValue
        Season objSeason = Season.SUMMER;//objSeason - объект, который вернул Season.SUMMER
        String result = objSeason.getSeason();
        System.out.println(result);

        //Пример 2 обращение к свойству возвращаемого объекта
        System.out.println(Season.AUTUMN.getSeason());
    }

}
