package lesson8.classwork.example2.pro;

public enum Season {

    WINTER("Зима"),
    SPRING("Весна"),
    SUMMER("Лето"),
    AUTUMN("Осень");

    String seasonValue;

    Season (String seasonValue){
        this.seasonValue = seasonValue;
    }

    public String getSeason(){
        return seasonValue;
    }


}

