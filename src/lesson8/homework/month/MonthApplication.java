package lesson8.homework.month;

import java.util.Scanner;

public class MonthApplication {
    /**
     * Создайте ENUM со всеми месяцами года. И выведите названия месяца по номеру месяца в году.
     * Постарайтесь сделать со вложенным свойством как мы на уроке делали.
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите номер месяца: ");
        int numMonth = scan.nextInt();

        Month.getValueOfMonth(numMonth);
    }
}
