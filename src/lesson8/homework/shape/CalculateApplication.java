package lesson8.homework.shape;

import java.util.Scanner;

public class CalculateApplication {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите пожалуйста радиус круга");
        double inputRadius = scan.nextDouble();
        System.out.println("Получен следующий радиус: " +  inputRadius);
        Circle circle = new Circle(inputRadius);
        System.out.println("Площадь кругу 1 составляет: " + circle.getArea());

        Circle circle2 = new Circle(99);
        System.out.println("Площадь кругу 2 составляет: " + circle2.getArea());

        Rectangle rectangle = new Rectangle(3, 4);
        System.out.println("Площадь квадрата 1 : " + rectangle.getArea());
    }
}
