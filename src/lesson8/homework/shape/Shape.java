package lesson8.homework.shape;

public interface Shape {

    double getArea();

}
