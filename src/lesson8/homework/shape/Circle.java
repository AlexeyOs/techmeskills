package lesson8.homework.shape;

public class Circle implements Shape {
    private double radius;
    private double pi = 3.14;

    public Circle(double radius){
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return radius * radius * pi;
    }
}
