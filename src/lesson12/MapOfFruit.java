package lesson12;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MapOfFruit {

    /**
     * Создать коллекцию HashMap<String, String>, занести туда 10 пар строк: арбуз – ягода, банан – трава, вишня – ягода, груша – фрукт, дыня – овощ, ежевика – куст, жень-шень – корень, земляника – ягода, ирис – цветок, картофель – клубень.
     * Вывести содержимое коллекции на экран, каждый элемент с новой строки
     */
    public static void main(String[] args) {
        Map<String, String> fruits = new HashMap<>();

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            String name = scanner.next();
            String typ = scanner.next();
            fruits.put(name, typ);
        }
        for (Map.Entry<String, String> elem:fruits.entrySet()) {
            System.out.println(elem);
        }

        System.out.println("Вывод через builder");

        for (Map.Entry elem:fruits.entrySet()) {
            StringBuilder builder = new StringBuilder();
            builder.append(elem.getKey()).append("-").append(elem.getValue());
            System.out.println(builder);
        }
    }
}
