package lesson12;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class SetOfFruit {
    /**
     * Создать коллекцию HashSet с типом элементов String.
     * Добавить в неё 10 строк: арбуз, банан, вишня, груша, дыня, ежевика, жень-шень, земляника, ирис, картофель.
     * Вывести содержимое коллекции на экран, каждый элемент с новой строки.
     * Посмотреть, как изменился порядок добавленных элементов.
     */
    public static void main(String[] args) {
        Set<String> fruits = new LinkedHashSet<>();
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < 10; i++){
            String fruit = scan.next();
            fruits.add(fruit);
        }
        System.out.println("Вывод через for");
        for (String fruit:fruits) {
            System.out.println(fruit);
        }
//        System.out.println("Вывод через while");
//        Iterator value = fruits.iterator();
//        while (value.hasNext()){
//            System.out.println(value.next());
//        }
    }

}
