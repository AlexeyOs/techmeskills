package lesson12;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class CalculationApplication {

    /**
     *  деление на ноль и обработка исключения
     */
    public static void main(String[] args) {
        Set set = new HashSet();
        set.iterator();

        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введите делимое ");
            int tmp = scan.nextInt();
            System.out.println("Введите делитель");
            int nul = scan.nextInt();
            System.out.println(tmp / nul);

        } catch (ArithmeticException ex) {

            try {
                System.out.println("ОШибка деления на ноль. ВВедите повторно");
                Scanner scan2 = new Scanner(System.in);
                System.out.println(scan2.nextInt() / scan2.nextInt());

            } catch (ArithmeticException ex2) {
                System.out.println("На ноль делить нельзя. Программа завершена");
            }
        }
    }


}
