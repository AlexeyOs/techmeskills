package lesson12;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class SetOfFruitPro {

    public static void main(String[] args) {

        Set<String> fruits = new LinkedHashSet<>();

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 10; i++) {
            String fruit = scanner.next();
            fruits.add(fruit);
        }

        System.out.println("Если хотите удалить элемент введите ДА, иначе НЕТ");
        String st = scanner.next();

        while (!st.equals("НЕТ")) {
            if (st.equals("ДА")) {
                System.out.println("Введите элемент который хотите удалить");
                String temp = scanner.next();
                fruits.remove(temp);
            }
            printAllFruits(fruits);
            System.out.println("Если хотите удалить элемент введите ДА, иначе НЕТ");
            st = scanner.next();
        }
    }
    static void printAllFruits(Set<String> fruits){
        for (String fruit: fruits) {
            System.out.println(fruit);
        }
    }

}
