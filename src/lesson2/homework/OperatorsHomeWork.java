package lesson2.homework;

import java.util.Random;
import java.util.Scanner;

public class OperatorsHomeWork {
    public static void main(String[] args) {
        task1();
        task2();
        task3InDifferentMethods();
        task3InOneMethod();
        task4();
    }

    /**
     * Даны 3 целых числа. Найти количество положительных чисел в исходном наборе.
     */
    private static void task1() {

        int[] mas = new int[]{3, -4, 8};

        System.out.print("Исходный массив: ");
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i] + " ");
        }

        int pos = 0;

        for (int i = 0; i < mas.length; i++) {
            if (mas[i] > 0) {
                pos++;
            }
        }
        System.out.println();
        System.out.println("Количество положительных чисел " + pos);
    }

    /**
     * Три целых числа вводятся из консоли. Найти количество положительных и отрицательных чисел
     * в исходном наборе.
     */
    static void task2() {
        Scanner sc = new Scanner(System.in);
        int[] mas = new int[3]; // массив из 3-x эллементов типа int

        for (int i = 0; i < mas.length; i++) {
            System.out.println("Введите значение элемента для позиции " + i + " ");
            mas[i] = sc.nextInt();
        }

        System.out.print("Исходный массив: ");
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i] + " ");
        }

        int pos = 0;
        int neg = 0;

        for (int i = 0; i < mas.length; i++) {
            if (mas[i] > 0) {
                pos++;
            }else if(mas[i]<0) { //чтобы учесть 0
                neg++;
            }
        }
        System.out.println();
        System.out.println("Количество положительных чисел " + pos);
        System.out.println("Количество отрицательных чисел " + neg);
    }

    /**
     * Найти минимум в массиве
     */
    public static void task3InDifferentMethods() {
        int[] mas = new int[10]; // массив из 10 эллементов типа int

        for (int i = 0; i < mas.length; i++) {
            mas[i] = RandomInt();// заполняет массив рандомными числами в границах 100
            System.out.println("mas [" + i + "]=" + mas[i] + ";");// выводим массив заполненный рандомными числами
        }
        min(mas); // минимум
    }

    private static int RandomInt() {
        Random rn = new Random();//создаем генератор случайных чисел
        return rn.nextInt(100);// снова передает управление объекту, который вызвал данный метод
    }

    /** поиск минимума */
    private static void min(int[] mas) {
        int minValue = mas[0];

        for (int i = 1; i < mas.length; i++) {
            if (mas[i] < minValue)
                minValue = mas[i];
        }
        System.out.println("Минимум: " + minValue);
    }

    /**
     * Второй вариант заполнения массива
     */
    private static void task3InOneMethod() {

        System.out.println("Введите длину массива");
        Scanner sc = new Scanner(System.in);
        int lengthArr = sc.nextInt();

        int arr[] = new int[lengthArr]; //массив из lengthArr эддементов типа int
        //int arr[] = new int[10]; // массив из 10-ти эллементов

        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) Math.round(Math.random() * 100); // Math.random() возвращает случайное число с плавающей запятой  [0,1)
            //Умножая это значение на 100, получаем случайное число в диапазоне [0,100)
            //Math.round() — округляем до целого
            System.out.println("arr [" + i + "]=" + arr[i] + ";");
        }

        int minValue = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < minValue)
                minValue = arr[i];
        }
        System.out.println("Минимум массива :" + minValue);
    }

    /**
     * (Дополнительно) В переменную записываете количество программистов.
     * В зависимости от количества программистов необходимо вывести правильно окончание.
     * Например: • 2 программиста • 1 программиста • 10 программистов • и т.д
     */
    private static void task4() {
        Scanner scanner  = new Scanner(System.in);
        System.out.println("Введите количество программистов:");
        int prog = scanner.nextInt();
        if(prog % 10 == 2 && (prog - 12) % 100 !=0 ||
                prog % 10 == 3 && (prog - 13) % 100 !=0 ||
                prog % 10 == 4 && (prog - 14) % 100 !=0){
            System.out.println(prog + " программиста");
        }else if(prog % 10 == 1 && (prog - 11) % 100 != 0){
            System.out.println(prog + " программист");
        }else if(prog < 0){
            System.out.println("Введите корректное число программистов!");
        }else {
            System.out.println(prog + " программистов");
        }
    }
}





