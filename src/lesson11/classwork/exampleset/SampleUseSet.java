package lesson11.classwork.exampleset;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SampleUseSet {

    public static void main(String[] args) {
        // Проверяет возможность добавить одинаковые строки
//        Set<String> collect = new HashSet<>();
//        collect.add("Petya");
//        collect.add("Petya");
//        collect.add("Petya2");
//
//        for (String elem : collect){
//            System.out.println(elem);
//        }

        //Проверяет возможность добавить одинаковые объекты типа Person
        // (Необходимо переопределить equals и hashcode у Person), чтобы работало корректно
        Scanner sc = new Scanner(System.in);
        Set<Person> persons = new HashSet<>();
        String value = sc.next();
        while (!value.equals("stop")){
            Person person = new Person(value);
            persons.add(person);
            value = sc.next();
        }
        for (Person element: persons) {
            System.out.println(element.getName());
        }

    }

}
