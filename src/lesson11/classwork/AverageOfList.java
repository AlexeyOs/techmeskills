package lesson11.classwork;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class AverageOfList {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputint = scanner.next();

        List<Integer> list = new ArrayList<>();
        while (!inputint.equals("stop")){
            try {
                list.add(Integer.valueOf(inputint));
            } catch (Exception ex){
                System.out.println("Вы ввели не корректное значение . Попробуйте ещё раз! ");
            }

            inputint = scanner.next();
        }

        int summelement = 0;

        for (Integer element : list) {
            summelement = summelement + element;

        }
        double resultat = summelement/list.size();
        System.out.println(resultat);
    }
}

