package lesson11.classwork.examplemap;

import java.util.*;

public class SampleUseMapOfPersonByName {

    /**
     * Вывести телефоны абонента(или абонентов) по номеру телефона
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<String, List<Person>> personsByPhone = new HashMap<>();
        //Сложный ввод значений из консоли
        System.out.println("Введите имя");
        String name = sc.next();
        System.out.println("Введите телефон");
        String phone = sc.next();
        while (!name.equals("stop") && !phone.equals("stop")){
            Person person = new Person(name, phone); // Создаём объект Person

            //Если в мапе нету значений для данного имени, то создаем список, если уже есть то достаём из мапы
            List<Person> persons = personsByPhone.get(name) == null ? new ArrayList<>() : personsByPhone.get(name);
            persons.add(person);
            personsByPhone.put(name, persons);

            System.out.println("Введите имя");
            name = sc.next();
            //Чтобы после команды stop не считывать телефон
            if (name.equals("stop")){
                break;
            }
            System.out.println("Введите телефон");
            phone = sc.next();
        }
        //Выводим все значения из map (в map persons есть ключи это имена, и значение это массив person-ов)
        // вот значения мы получаем методом values()
        for (List<Person> element: personsByPhone.values()) {
            System.out.println(element.toString());
        }

        System.out.println("Введите имя абонента,");
        //Получаем person из map где хранятся все persons по ключу ввиде имени
        List<Person> persons = personsByPhone.get(sc.next());
        //Достаём у person телефон
        for (Person person : persons) {
            System.out.println(person.getPhoneNumber());
        }

    }
}
