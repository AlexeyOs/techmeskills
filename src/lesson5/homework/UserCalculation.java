package lesson5.homework;

import java.util.Random;

public class UserCalculation {
    /**
     * Создайте класс User с полями:
     *  имя
     *  логин
     *
     *  В цикле создайте массив с пользователями(User).
     */
    public static void main(String[] args) {
        String[] name = new String[]{"Оля","Катя", "Петя", "Паша", "Лиза", "Алиса", "Коля", "Саша", "Даша","Таня"};
        String [] login = new String[]{"Star234", "Cat3", "P2424", "122QA","Zrew", "Sky","Dog132", "CatX","Dwde23", "Qze"};

        User[] users = new User[10];

        for(int i = 0; i<users.length; i++){
            User user = new User(); //Создаем объект User
            user.setLogin(login[getRandom(10)]);//передаем значение из массива login по случайному индексом
            user.setName(name[i]); //передаем значение из массива name, по индексу i
            users[i] = user; //кладём нового пользователя в массив
        }
        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i].toString());
        }

    }
    private static int getRandom(int maxLimit){
        Random random = new Random();
        return random.nextInt(maxLimit);
    }
}
