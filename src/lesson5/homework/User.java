package lesson5.homework;

public class User {
    public String name; // поле имя
    public String login; // поле логин


    public void setLogin(String login) {
        this.login = login; // присваиваем свойству login значение входного параметра
    }

    public void setName(String name) {
        this.name = name;// присваиваем свойству name значение входного параметра
    }

    @Override
    public String toString() {
        return "Имя " + name + " Логин " + login;
    }
}
