package lesson6.homework;

public class Student {

    Student(String name, int group, int grade){
        this.name = name;
        this.group = group;
        this.grade = grade;
    }

    public String name;
    public int group;
    public int grade;

    /**
     * Тот самый метод, который будет выводить всю информацию о студенте.
     */
    public void studentsThirdGroup()
    {
        if(group == 3) {
            System.out.println(toString());
        }
    }

    @Override
    public String toString()
    {
        return "Name " + name + " group " + group + " Grade " + grade;
    }

}
