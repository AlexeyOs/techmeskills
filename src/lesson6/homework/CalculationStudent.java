package lesson6.homework;

import java.util.Random;

public class CalculationStudent {

    /**
     * Создайте метод класса студент, который будет выводить всю информацию о студенте.
     * Выведите информацию о всех студентах из 3 группы в консоль.
     */
    public static void main(String[] args)
    {

        String[] name = new String[]{"Sasha", "Pasha", "Dasha", "Jooly", "Natasha"};
        Student[] students = new Student[14];

        for (int i = 0; i < students.length; i++) {
            Student student = new Student(name[getRandom(5)], getRandom(4), getRandom(11));
            students[i] = student;
        }

        System.out.println("Список всех студентов:");
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].toString());
        }

        System.out.println("\nСписок студентов 3 группы:");
        for (int i = 0; i < students.length; i++) {
            if (students[i].group == 3)
                students[i].studentsThirdGroup();
        }
    }

    private static int getRandom(int maxLimit) {
        Random random = new Random();
        return random.nextInt(maxLimit);
    }
}