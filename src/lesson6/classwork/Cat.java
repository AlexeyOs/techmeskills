package lesson6.classwork;

public class Cat {
    String name;
    int age;
    int gramm;

    Cat(String name, int age, int gramm){
        this.name = name;
        this.age = age;
        this.gramm = gramm;
    }

    public boolean checkSatiety(){
        return gramm > 100;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gramm=" + gramm +
                '}';
    }
}
