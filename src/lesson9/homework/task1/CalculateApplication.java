package lesson9.homework.task1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class CalculateApplication {
    /**
     * 1) Задания связанный с фигурами
     *  а) Вводить параметры такие как радиус, стороны прямоугольника из консоли и обрабатывать некорректные значения
     *  б) Сделать расчёт периметра для круга и прямоугольника
     */
    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введите пожалуйста радиус круга 1");
            double inputRadiusFirstCircle = scan.nextDouble();
            System.out.println("Получен следующий радиус круга 1: " +  inputRadiusFirstCircle);
            Circle circle = new Circle(inputRadiusFirstCircle);
            System.out.println("Площадь круга 1 составляет: " + circle.getArea());
            System.out.println("Периметр круга 1 составляет: " + circle.getPerimeter());

            System.out.println("Введите пожалуйста радиус круга 2");
            double inputRadiusSecondCircle = scan.nextDouble();
            System.out.println("Получен следующий радиус круга 2: " +  inputRadiusSecondCircle);
            Circle circle2 = new Circle(inputRadiusSecondCircle);
            System.out.println("Площадь круга 2 составляет: " + circle2.getArea());
            System.out.println("Периметр круга 2 составляет: " + circle2.getPerimeter());

            System.out.println("Введите пожалуйста ширину прямоугольника");
            double width = scan.nextDouble();
            System.out.println("Получена следующая ширина прямоугольника: " + width);
            System.out.println("Введите пожалуйста высоту прямоугольника");
            double height = scan.nextDouble();
            System.out.println("Получена следующая высоту прямоугольника: " + height);
            Rectangle rectangle = new Rectangle(width, height);
            System.out.println("Площадь квадрата 1 : " + rectangle.getArea());
            System.out.println("Периметр квадрата 1 : " + rectangle.getPerimeter());
        } catch (InputMismatchException inputMismatchException){
            System.out.println("Вы ввели не корректные данные");
        }
    }
}
