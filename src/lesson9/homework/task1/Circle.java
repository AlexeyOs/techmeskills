package lesson9.homework.task1;

public class Circle implements Shape {
    private double radius;
    private double pi = 3.14;

    public Circle(double radius){
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return radius * radius * pi;
    }

    @Override
    public double getPerimeter() {
        return pi * 2 * radius;
    }
}
