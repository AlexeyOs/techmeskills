package lesson9.homework.task1;

public interface Shape {

    double getArea();
    double getPerimeter();

}
