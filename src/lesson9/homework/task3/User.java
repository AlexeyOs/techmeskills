package lesson9.homework.task3;

import java.util.InputMismatchException;

public class User {
    private String name;
    private String surname;

    public void setName(String name){
        //Можно было использовать contains как в классной работе
//        if (name.contains("1") ||
//                name.contains("2") ||
//                name.contains("3") ||
//                name.contains("4") ||
//                name.contains("5")){
//            System.out.println("Данные введены неверно");
        //Но более правильно использовать регулярные выражения
        if (name.matches(".*\\d+.*")) {
            this.name = name;
        } else{
            System.out.println("Данные содержат цифры");
            throw new InputMismatchException();

        }
    }

    public void setSurname(String surname){

        if (surname.matches(".*\\d+.*")) {
            this.surname = surname;
        }
        else{
            System.out.println("Данные содержат цифры");
            throw new InputMismatchException();

        }
    }

    public String getName(){
        return name;
    }

    public String getSurname() {
        return surname;
    }
}

