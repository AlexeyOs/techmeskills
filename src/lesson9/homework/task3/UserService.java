package lesson9.homework.task3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserService {
    /**
     * 3) Сделать у Пользвователя свойство Фамилия, и сделать валидацию, чтобы там не было цифр.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите имя");
        String name = scanner.next();
        System.out.println("Введите фамилию");
        String surname = scanner.next();

        User user = new User();

        try {
            user.setName(name);
            user.setSurname(surname);
        } catch (InputMismatchException e) {
           System.out.println("Введи нормальное значение имени либо программа закончится");
           name = scanner.next();
           surname = scanner.next();
           user.setName(name);
           user.setSurname(surname);
        }

        System.out.println("Корректные имя и фамилия:  " + user.getName() + " " + user.getSurname());
    }
}
