package lesson1.homework;

import java.util.Scanner;

public class InOneMethod {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        // Даны два числа вывести меньшее из них
        System.out.println("Введите целые числа а и b");
        int a = sc.nextInt(); //nextInt считывает и возвращает введенное число переменной
        int b = sc.nextInt();
        if (a > b) {
            System.out.println(b);
        } else {
            System.out.println(a);

        }

       //даны  два числа, вывести их сумму
        System.out.println("Введите целое число");
        int  c = sc.nextInt();
        System.out.println("Введите целое число");
        int d = sc.nextInt();
        System.out.println("Cумма чисел = "+(c+d));

        //Даны три целочисленных числа и с плавающей точкой - вывести их произведение
       System.out.println("Введите целое число");
        int a1 = sc.nextInt();
        System.out.println("Введите целое число");
        int a2 = sc.nextInt();
        System.out.println("Введите число типа double");
        double f = sc.nextDouble();
        double pr = a1*a2*f;
        System.out.println("Произведение чисел= "+pr);

        //Даны два числа вывести остаток от деления этих чисел
        System.out.println("Введите целое число");
        int n1 = sc.nextInt();
        System.out.println("Введите целое число");
        int n2 = sc.nextInt();
        System.out.println("Остаток от деления " + n1 % n2);

        //Даны два числа с плавающей точкой.Преобразовать их сумму к целочисленному значению
        double d1=3.14;
        double d2=5.67;
        int sum = (int)Math.round(d1) + (int)Math.round(d2); //функция округления
        System.out.println("Целочисленное значение суммы=" +sum);

    }
}

