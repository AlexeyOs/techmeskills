package lesson1.classwork;

public class BestCalculation {

    public static void main(String[] args) {
        changeTwo();
        chargeTwoWithParam(3,4.6);
    }

    private static void taskCalculationOne(){
        //Задание 1	Даны 2 числа. Вывести большее из них.
        int calc1 = 6;
        float calc2 = 5.5f;

        if (calc1 > calc2) {
            System.out.println(calc1);
        } else {
            System.out.println(calc2);
        }
    }

    private static void changeTwo(){
        //Пеример 2 Преобразование
        int a = 3;
        double b = 4.6;
        int c =  a + (int) b;
        System.out.println(c);
    }

    private static void chargeTwoWithParam(int a, double b) {
        int c =  a + (int) b;
        System.out.println(c);
    }
}
