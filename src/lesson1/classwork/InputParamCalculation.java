package lesson1.classwork;

import java.util.Scanner;

public class InputParamCalculation {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inputNum = scanner.nextInt();
        System.out.println("Вы ввели число " + inputNum);
    }

}
