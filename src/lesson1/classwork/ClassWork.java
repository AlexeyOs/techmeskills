package lesson1.classwork;

public class ClassWork {

    private static void exampleFor(){
        int[] arr = new int[]{1,2,3};
        System.out.println("For each");
        for (int a:arr){
            System.out.print(a + " ");
        }
        System.out.println();
        System.out.println("Просто For");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i] + " ");
        }
    }
}
