package lesson10.classwork.examplemodel;

public class CallService {
    public static void main(String[] args) {
        Model model = new Model();
        //Вернули строку из Model
        String testValue = model.getString();
        //Присвоили свойству number номер
        model.setNomer(1);
        //Возвращаем значение number из Model
        int number = model.getNomer();
        System.out.println(number);

        Model model2 = new Model(2);
        System.out.println(model2.getNomer());

    }
}
