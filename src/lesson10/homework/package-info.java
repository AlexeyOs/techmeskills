package lesson10.homework;

/**
 * 1) Сделать класс модели и потренировать присваивать свойства объекта и их возвращать
 * Как мы делали в начале занятия
 *
 * 2) Вводить значения из консоли и выводить минимум
 * Используя одну из реализаций List(LinkedList и ArrayList)
 *
 * 3) Ввести n строк с консоли. Вывести на консоль те строки, длина которых больше средней, а также длину.
 * для вывода результат используйте StringBuilder
 */