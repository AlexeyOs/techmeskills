package lesson10.homework.task2;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class LinkedListMin {
    /**
     * Вводить значения из консоли и выводить минимум
     * Используя одну из реализаций List(LinkedList и ArrayList)
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите элементы массива. Чтобы прекратить ввод напишите stop");
        List<Integer> numbers = new LinkedList<>();
        while (true) {
            String n = scanner.next();
            if (n.equals("stop"))
                break;
            numbers.add(Integer.valueOf(n));
        }
        int minValue = numbers.get(0);
        for (Integer number : numbers) {
            if (number < minValue) {
                minValue = number;
            }
        }
        System.out.println(minValue);
    }
}
